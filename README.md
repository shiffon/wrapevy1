## WrapEVY1について ##

**WarpEVY1** は[スイッチサイエンス社](http://www.switch-science.com/catalog/1490/)から発売されているeVY1の操作を簡略化するためものです。

### WrapEVY1の動作環境 ###
- .net Framework3.5以降が必要です。
- VS Express for Desktopで動作確認をしていますがVisualStudioC#2008以降であれば動くと思います。
- eVY1へのメッセージの送信にNext MIDI Projectのクラスを利用していますので、[Next MIDI Project](http://starway.s234.xrea.com/wordpress/?page_id=247)からライブラリをダウンロードしてください。

### WrapEVY1の利用方法 ###
- プロジェクトをダウンロードする。
- ダウンロードしたプロジェクトを利用したいプロジェクトに追加する。
- Next MIDI Projectのライブラリを全て参照に追加する。

### EVY1の操作方法 ###

- Sample.csを参照（ドレミファソラシドの音で"さくらさくらさく"と歌います）
- eVY1固有の所以外はNext MIDI Projectの操作となるため、詳細は[そちら](http://starway.s234.xrea.com/wordpress/?page_id=285)へ

### ライセンス ###

- 本ソース一式はMITライセンスで公開しています。
- This software is released under the MIT License, see LICENSE.txt.
- 何かあれば[ここ](shiffon.1f@gmail.com)まで。

### 著作権表示 ###

本プロジェクトでは以下のライブラリを利用しています。

- [Next MIDI Project](http://starway.s234.xrea.com/wordpress/?page_id=247)
