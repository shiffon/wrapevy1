﻿using NextMidi.DataElement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace WrapEVY1
{
    class Smaple
    {
        private void PlaySong()
        {
            OperateEVY1 wrapEVY1 = new OperateEVY1();

            string paString = wrapEVY1.ConvertHiraganaToPAData("さくらさくら");
            byte[] paByte = wrapEVY1.ConvetPADataToByteLyrics(paString);

            wrapEVY1.OpenMidiOutPort("eVY1 MIDI");

            wrapEVY1.SendByteLyrics(paByte);

            foreach (byte n in new byte[8] { 60, 62, 64, 65, 67, 69, 71, 72 })
            {
                wrapEVY1.SendMusicOn(n,112);
                wrapEVY1.SendVibrate(60);

                Thread.Sleep(n != 72 ? 1000 : 1500);
                wrapEVY1.SendMusicOff(n);
            }

            wrapEVY1.CloseMidiOutPort();
        }
    }
}
