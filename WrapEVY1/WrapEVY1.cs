﻿using NextMidi.DataElement;
using NextMidi.MidiPort.Output;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace WrapEVY1
{
    /// <summary>
    /// EVY1の操作をラッピングしたクラス
    /// </summary>
    public class OperateEVY1
    {
        public MidiOutPort midiOutPort {get; set;}
        private string kanaString = "あ,い,う,え,お,か,き,く,け,こ,さ,し,す,せ,そ,た,ち,つ,て,と,な,に,ぬ,ね,の,は,ひ,ふ,へ,ほ,ま,み,む,め,も,ら,り,る,れ,ろ,が,ぎ,ぐ,げ,ご,ざ,じ,ず,ぜ,ぞ,だ,ぢ,づ,で,ど,ば,び,ぶ,べ,ぼ,ぱ,ぴ,ぷ,ぺ,ぽ,や,ゆ,よ,わ,ゐ,ゑ,を,ふぁ,つぁ,うぃ,すぃ,ずぃ,つぃ,てぃ,でぃ,ふぃ,とぅ,どぅ,いぇ,うぇ,きぇ,しぇ,ちぇ,つぇ,てぇ,にぇ,ひぇ,みぇ,りぇ,ぎぇ,じぇ,でぇ,びぇ,ぴぇ,ふぇ,うぉ,つぉ,ふぉ,きゃ,しゃ,ちゃ,てゃ,にゃ,ひゃ,みゃ,りゃ,ぎゃ,じゃ,でゃ,びゃ,ぴゃ,ふゃ,きゅ,しゅ,ちゅ,てゅ,にゅ,ひゅ,みゅ,りゅ,ぎゅ,じゅ,でゅ,びゅ,ぴゅ,ふゅ,きょ,しょ,ちょ,てょ,にょ,ひょ,みょ,りょ,ぎょ,じょ,でょ,びょ,ぴょ";
        private string alphabetString = "a,i,M,e,o,k a,k' i,k M,k e,k o,s a,S i,s M,s e,s o,t a,tS i,ts M,t e,t o,n a,J i,n M,n e,n o,h a,C i,p\\ M,h e,h o,m a,m' i,m M,m e,m o,4 a,4' i,4 M,4 e,4 o,g a,g' i,g M,g e,g o,dz a,dZ i,dz M,dz e,dz o,d a,dZ i,dz M,d e,d o,b a,b' i,b M,b e,b o,p a,p' i,p M,p e,p o,j a,j M,j o,w a,w i,w e,o,p\\ a,ts a,w i,s i,dz i,ts i,t' i,d' i,p\' i,t M,d M,j e,w e,k' e,S e,tS e,ts e,t' e,J e,C e,m' e,4' e,g' e,dZ e,d' e,b' e,p' e,p\\ e,w o,ts o,p\\ o,k' a,S a,tS a,t' a,J a,C a,m' a,4' a,N' a,dZ a,d' a,b' a,p' a,p\' a,k' M,S M,tS M,t' M,J M,C M,m' M,4' M,g' M,dZ M,d' M,b' M,p' M,p\' M,k' o,S o,tS o,t' o,J o,C o,m' o,4' o,N' o,dZ o,d' o,b' o,p' o"; 
        private Dictionary<string, string> paDictionary = new Dictionary<string, string>();

        /// <summary>
        /// MIDIOUTPortを開く。
        /// </summary>
        /// <param name="midiOutPortName">MIDIOutPort名称。省略の場合はデフォルトを開く。</param>
        /// <returns>0:Success -1:Failure</returns>
        public int OpenMidiOutPort(string midiOutPortName = "")
        {
            if( midiOutPortName.Length == 0 )
            {
                midiOutPort = new MidiOutPort(0);
            }
            else
            {
                midiOutPort = new MidiOutPort(midiOutPortName);
            }
            
            try
            {
                midiOutPort.Open();
                //midiOutPort.Send(new ProgramEvent(4));
                //Thread.Sleep(500);
            }
            catch
            {
                return -1;
            }
            return 0;
        }

        /// <summary>
        /// MIDIOUTPortを閉じる
        /// </summary>
        public void CloseMidiOutPort()
        {
            if (midiOutPort != null)
            {
                midiOutPort.Close();
            }
        }

        /// <summary>
        /// 歌詞のバイトデータを送信する。
        /// </summary>
        /// <param name="byteLyrics">歌詞のバイトデータ</param>
        public void SendByteLyrics(byte[] byteLyrics)
        {
            ExclusiveEvent exclusiveEvent = new ExclusiveEvent(byteLyrics);
            exclusiveEvent.Channel = 1;
            midiOutPort.Send(exclusiveEvent);
            //Thread.Sleep(1000);
        }

        /// <summary>
        /// 音を鳴らす。
        /// </summary>
        /// <param name="note">ノート</param>
        /// <param name="velocity">音量</param>
        public void SendMusicOn(byte note, byte velocity)
        {
            NoteOnEvent noteOnEvent = new NoteOnEvent();
            noteOnEvent.Velocity = velocity;
            noteOnEvent.Note = note;
            midiOutPort.Send(noteOnEvent);
        }

        /// <summary>
        /// 音を止める。
        /// </summary>
        /// <param name="note">ノート</param>
        public void SendMusicOff(byte note)
        {
            midiOutPort.Send(new NoteOffEvent(note));
        }

        /// <summary>
        /// ビブラートをかける。
        /// </summary>
        /// <param name="depth">ビブラートの深さ</param>
        public void SendVibrate(byte depth)
        {
            ControlEvent controlEvent = new ControlEvent();
            controlEvent.Number = 0x01;
            controlEvent.Value = depth;
            midiOutPort.Send(controlEvent);
        }

        /// <summary>
        /// ひらがなで記述した歌詞をNSX-1の仕様書記載のアルファベットに変換する。
        /// </summary>
        /// <param name="hiraganaWord">ひらがなで記述した歌詞。複数文字の場合はカンマで区切ること</param>
        /// <returns>NSX-1の仕様書記載のアルファベット。複数文字の場合はカンマ区切りで返却</returns>
        public string ConvertHiraganaToPAData(string hiraganaWord)
        {
            StringBuilder convertPA = new StringBuilder();

            if( paDictionary.Count == 0 )
            {
                string[] strKanaSplit = kanaString.Split(',');
                string[] strAlphabetSplit = alphabetString.Split(',');

                for( int i = 0; i < strKanaSplit.Count(); i++ )
                {
                    paDictionary.Add( strKanaSplit[i], strAlphabetSplit[i]);
                }
            }

            foreach (char splitWord in hiraganaWord)
            {
                if( paDictionary.ContainsKey( splitWord.ToString() ) == true )
                {
                    convertPA.Append( paDictionary[splitWord.ToString()] );
                    convertPA.Append(",");
                }
            }
            convertPA.Remove( convertPA.Length-1, 1);

            return convertPA.ToString();
        }

        /// <summary>
        /// NSX-1の仕様書記載のアルファベットを歌詞のバイトデータに変換する。
        /// </summary>
        /// <param name="paWord">NSX-1の仕様書記載のアルファベット</param>
        /// <returns>歌詞のバイトデータ</returns>
        public byte[] ConvetPADataToByteLyrics(string paWord)
        {
            byte[] headHexArray = { 0xF0, 0x43, 0x79, 0x09, 0x00, 0x50, 0x10 };
            string headString = Encoding.ASCII.GetString(headHexArray);

            byte[] footHexArray = { 0x00, 0xF7 };
            string footString = Encoding.ASCII.GetString(footHexArray);

            paWord = headString + paWord + footString;

            byte[] exclusiveByteArray = System.Text.Encoding.ASCII.GetBytes(paWord);
            exclusiveByteArray[0] = 240;
            exclusiveByteArray[exclusiveByteArray.Length-1] = 247;

            return exclusiveByteArray;
        }

        /// <summary>
        /// NSX-1の仕様書から歌詞の日本語とアルファベットを分離
        /// </summary>
        public void CreatePAData()
        {
            StringBuilder strBuilder = new StringBuilder();
            string line;
            using (StreamReader sr = new StreamReader("../../../../WrapEVY1/WrapEVY1/PAList.txt", Encoding.GetEncoding("utf-8")))
            {
                while ((line = sr.ReadLine()) != null)
                {
                    strBuilder.Append(line);
                }
            }

            StringBuilder strKanaBuilder = new StringBuilder();
            StringBuilder strPABuilder = new StringBuilder();
            string orgPAString = strBuilder.ToString();
            foreach (char word in orgPAString)
            {
                if (Regex.IsMatch(word.ToString(), @"^[\u3040-\u309F]*$") == true)
                {
                    if (word == '\u3041' || word == '\u3043' || word == '\u3045' || word == '\u3047' || word == '\u3049' ||
                        word == '\u3063' || word == '\u3083' || word == '\u3085' || word == '\u3087')
                    {
                        strKanaBuilder.Remove(strKanaBuilder.Length - 1, 1);
                    }
                    else
                    {
                    }
                    strKanaBuilder.Append(word);
                    strKanaBuilder.Append(",");

                    string work = strPABuilder.ToString();
                    if ( work.Length != 0 && work[work.Length-1].Equals(',') == false )
                    {
                        strPABuilder.Append(",");
                    }
                }
                else
                {
                    strPABuilder.Append(word);
                }
            }
        }
    }
}
